import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PageNiButtonsComponent } from './buttons.component';

describe('ButtonsComponent', () => {
  let component: PageNiButtonsComponent;
  let fixture: ComponentFixture<PageNiButtonsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PageNiButtonsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PageNiButtonsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
