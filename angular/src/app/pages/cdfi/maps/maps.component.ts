import { Component, OnInit } from '@angular/core';
// region import
import { DomSanitizer } from '@angular/platform-browser';
import { Headers, Http, Response } from '@angular/http';
import { CfdiServiceService } from "app/layouts/services/cfdi-service.service";
//endregion import
@Component({
      selector: 'app-maps',
      templateUrl: './maps.component.html',
      styleUrls: ['./maps.component.scss']
})
export class MapsComponent implements OnInit {
      // region variables
      private displayString: object;
      // private htmlpage2;
      private aAddress: Array<String> = [];
      // endregion variables
      constructor(private sanitizer: DomSanitizer, private webservice: CfdiServiceService) {

        // console.log('test');
            this.webservice.ExecuteGet('CFDI').subscribe(
                  (data) => {
                        if (data.error === 0 || data.error === '0') {
                              alert(data.errorDescripcion);
                        } else {
                              this.lfGetAddress(data);
                              // console.dir(data)
                        }
                  }
            );


            //  this.test = htmlpage2;
      }

      ngOnInit() {

      }

      lfGetAddress(data) {

            // data.forEach((item) => {

            //       let searchaddress = item.emisor.domicilio.estado === null ? '' : ' ' + item.emisor.domicilio.estado;
            //       searchaddress += (item.emisor.domicilio.municipio === null) ? '' : ' ' + item.emisor.domicilio.municipio;
            //       searchaddress += (item.emisor.domicilio.colonia === null) ? '' : ' ' + item.emisor.domicilio.colonia;
            //       searchaddress += (item.emisor.domicilio.calle === null) ? '' : ' ' + item.emisor.domicilio.calle;

            //       let tmp = searchaddress.trim().length;
            //       if (tmp > 0) {
            //             // console.log(item.emisor.id + '     ' + searchaddress);
            //             this.aAddress.push(searchaddress);
            //       }
            // });

            // ES6
            data.map(item => {
                  let searchaddress = item.emisor.domicilio.estado === null ? '' : ' ' + item.emisor.domicilio.estado;
                  searchaddress += (item.emisor.domicilio.municipio === null) ? '' : ' ' + item.emisor.domicilio.municipio;
                  searchaddress += (item.emisor.domicilio.colonia === null) ? '' : ' ' + item.emisor.domicilio.colonia;
                 // searchaddress += (item.emisor.domicilio.calle === null) ? '' : ' ' + item.emisor.domicilio.calle;

                  const tmp = searchaddress.trim().length;
                  if (tmp > 0) {
                        // console.log(item.emisor.id + '     ' + searchaddress);
                        this.aAddress.push(searchaddress);
                  }
            });

            // this.lfGetLatLong(this.aAddress[10].toString());
            this.lfGetLatLong(this.aAddress);

      }

      lfGetLatLong(listAddress) {

          console.dir(listAddress);
          alert('test');

            const htmlpage2 = `
    <!DOCTYPE html>
    <html>
    <head>
          <title>Simple Map</title>
          <meta name="viewport" content="initial-scale=1.0">
          <meta charset="utf-8">
          <style>
                #map {
                      height: 100%;
                }
                html,
                body {
                      height: 100%;
                      margin: 0;
                      padding: 0;
                }
          </style>
    </head>
    <body>
          <div id="map"></div>
          <script>
                var map;
                var markers = [];
                var geocoder;
               var tmp = (${listAddress});
                function initMap() {
                      var myLatlng = { lat: 23.2635847, lng: -101.2814849 };
                      map = new google.maps.Map(document.getElementById('map'), {
                            center: myLatlng,
                            zoom: 6
                      });
                      map.addListener('click', function (event) {
                            addMarker(event.latLng);
                      });
                      // search direction

                      geocoder = new google.maps.Geocoder();
                      console.dir(tmp.length);

                }
                function addMarker(location) {
                      deleteMarkers();
                      var marker = new google.maps.Marker({
                            position: location,
                            map: map
                      });
                      markers.push(marker);

                            geocoder.geocode( { 'address': element}, function(results, status) {
                              if (status == 'OK') {
                               console.dir(results);
                                console.dir(results[0].geometry.location.lat());
                                //console.dir(results[0].geometry.location);
                                map.setCenter(results[0].geometry.location);
                                var marker = new google.maps.Marker({
                                    map: map,
                                    position: results[0].geometry.location
                                });
                              } else {
                                alert('Geocode was not successful for the following reason: ' + status);
                              }
                            });

                }
                function deleteMarkers() {
                      clearMarkers();
                      markers = [];
                }
                function clearMarkers() {
                      setMapOnAll(null);
                }
                function setMapOnAll(map) {
                      for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(map);
                      }
                }
          </script>
          <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBSRUW5pYvODm4xuX6_gZC2EcPbxm9kdjQ&callback=initMap" async
                defer></script>
    </body>

    </html>
    `;
            //     this.htmlpage2 = htmlpage2;
            this.displayString = this.sanitizer.bypassSecurityTrustHtml(htmlpage2);

      }
}
