import { TestBed, inject } from '@angular/core/testing';

import { CfdiServiceService } from './cfdi-service.service';

describe('CfdiServiceService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CfdiServiceService]
    });
  });

  it('should be created', inject([CfdiServiceService], (service: CfdiServiceService) => {
    expect(service).toBeTruthy();
  }));
});
