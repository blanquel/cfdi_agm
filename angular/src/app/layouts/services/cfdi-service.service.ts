import { Injectable } from '@angular/core';
import { Http, RequestOptions, Response, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class CfdiServiceService {

  constructor(private http: Http) { }
  // local
  private itUrl = 'http://services20180226080119.azurewebsites.net/api';
  private headers = new Headers({ 'Content-Type': 'application/json' });
  private options = new RequestOptions({ headers: this.headers });

  // #region consume service Generic
  Execute(obj, address) {
    const objRequest = JSON.parse(obj);
    const rqst = this.http.post(`${this.itUrl}/${address}`, objRequest, this.options)
      .map(res => {
        return res.json();
      });
    return rqst;
  }
  ExecuteGet( address) {
    // const objRequest = JSON.parse(obj);
    const rqst = this.http.get(`${this.itUrl}/${address}`, this.options)
      .map(res => {
        return res.json();
      });
    return rqst;
  }
  // 
  UploadFile(formData, address) {
    const rqst = this.http.post(`${this.itUrl}/${address}`, formData)
      .map(res => {
        return res.json();
      });
    return rqst;
  }
  // #endregion Generic

}
